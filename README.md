DSR (Digital Sales Reports) Dashboard (react) and Backend API (django-rest-framework).

Front-end allows filtering by dates, using the date range picker.
Also, the currency type, periodicity, and territory.
These can be filtered by the drop-down selects.

All possible currencies and territories are shown, even if reports for those do not exist.

All the statistics types on the charts can be changed using the selector in the top right.

The lower charts show the spread of currencies, and territories for all of the DSR reports.

## backend can be run in root directory with:
`python manage.py testserver api/fixtures/dummy_data.json`

## to run the frontend, open a new terminal window and run:
`cd frontend`

the node modules need to be installed, so run
`npm install`

followed by

`npm start`

the frontend is now accessible at http://localhost:3000

## example screenshots

![top screenshot](screenshot1.png)

![bottom screenshot](screenshot2.png)
