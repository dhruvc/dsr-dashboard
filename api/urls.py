from . import views

from django.conf.urls import url

urlpatterns = [
    url(r'^api/dsrs$', views.all_dsrs, name='dsrs'),
    url(r'^api/dsrs/count$', views.count, name='count'),
    url(r'^api/dsrs/(?P<pk>[0-9]+)/$', views.dsr,
        name='single-dsr'),
    url(r'^api/stats/(?P<pk>[0-9]+)/$', views.statistics,
        name='single-stats'),
    url(r'^api/stats$', views.all_stats, name='stats'),
    url(r'^api/currency/(?P<code>[\w]+)/$', views.currency,
        name='currency'),
    url(r'^api/currencies/$', views.all_currencies,
        name='currencies'),
    url(r'^api/dsr_currencies/$', views.dsr_currencies,
        name='dsr-currencies'),
    url(r'^api/dsr_territories/$', views.dsr_territories,
        name='dsr-territories'),
    url(r'^api/territory/(?P<code>[\w]+)/$', views.territory,
        name='territory'),
    url(r'^api/territories/$', views.all_territories,
        name='territories'),
    url(r'^api/period/(?P<code>[\w]+)/$', views.period,
        name='period'),
    url(r'^api/periods/$', views.all_periods, name='periods'),
    url(r'^api/date-range/all$', views.all_stats,
        name='date-range-all'),
    url(r'^api/date-range/(?P<pk>[0-9]+)/$',
        views.date_range_month, name='date-range-month'),
    url(r'^api/date/(?P<start>[\w\-\.]+)/(?P<end>[\w\-\.]+)/$',
        views.date_range, name='date-range'),
]
