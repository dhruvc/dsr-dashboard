from django.test import TestCase
from rest_framework.test import APIRequestFactory


class DummyTest(TestCase):
    pass


class CurrenciesTest(TestCase):
    factory = APIRequestFactory()

    request = factory.get(
        '/api/currencies/'
    )
