from api import models, serializers

from rest_framework.decorators import api_view
from rest_framework.response import Response


# view to return total count
@api_view(['GET'])
def count(request):
    return Response(headers={'X-Total-Count': models.DSR.objects.count()})


# view to return a single dsr by private key
@api_view(['GET'])
def dsr(request, pk):
    try:
        dsr = models.DSR.objects.get(pk=pk)
        data = serializers.DSRSerializer(dsr).data
    except models.DSR.DoesNotExist:
        data = None
    return Response(
        {'data': data, },
        headers={'X-Total-Count': 1 if data else 0}
    )


# return the statistics for a dsr by private key
@api_view(['GET'])
def statistics(request, pk):
    try:
        stats = models.Statistics.objects.get(pk=pk)
        data = serializers.StatisticsSerializer(stats).data
    except models.DSR.DoesNotExist:
        data = None
    return Response(data)


# return all dsr objects
@api_view(['GET'])
def all_dsrs(request):
    dsrs = models.DSR.objects.all()
    data = serializers.DSRSerializer(dsrs, many=True).data
    return Response(data)


# return all currencies
@api_view(['GET'])
def all_currencies(request):
    currencies = models.Currency.objects.all()
    data = serializers.CurrencySerializer(currencies, many=True).data
    return Response(data)
# return all currencies

# return currencies for all dsrs
@api_view(['GET'])
def dsr_currencies(request):
    all_currencies = models.Currency.objects.filter(dsr__isnull=False)
    data = serializers.CurrencySerializer(all_currencies, many=True).data
    return Response(data)


# return territories for all dsrs
@api_view(['GET'])
def dsr_territories(request):
    all_currencies = models.Territory.objects.filter(dsr__isnull=False)
    data = serializers.TerritorySerializer(all_currencies, many=True).data
    return Response(data)


# return dsrs filtered by specific currency code
@api_view(['GET'])
def currency(request, code):
    try:
        stats = models.Statistics.objects.filter(dsr__currency__code=code)
        data = serializers.StatisticsSerializer(stats, many=True).data
    except models.Statistics.DoesNotExist:
        data = None
    return Response(data)


# reutrn all statistics for all dsrs
@api_view(['GET'])
def all_stats(request):
    dsrs = models.Statistics.objects.all()
    data = serializers.StatisticsSerializer(dsrs, many=True).data
    return Response(data)


# return all territories
@api_view(['GET'])
def all_territories(request):
    currencies = models.Territory.objects.all()
    data = serializers.TerritorySerializer(currencies, many=True).data
    # print(data)
    return Response(data)


# return all periods
@api_view(['GET'])
def all_periods(request):
    currencies = models.Periodicity.objects.all()
    data = serializers.PeriodicitySerializer(currencies, many=True).data
    return Response(data)


# return dsrs filtered by specific territory id
@api_view(['GET'])
def territory(request, code):
    try:
        stats = models.Statistics.objects.filter(dsr__territory__id=code)
        data = serializers.StatisticsSerializer(stats, many=True).data
    except models.Statistics.DoesNotExist:
        data = None
    return Response(data)


# return dsrs filtered by specific period id
@api_view(['GET'])
def period(request, code):
    try:
        stats = models.Statistics.objects.filter(dsr__periodicity__id=code)
        data = serializers.StatisticsSerializer(stats, many=True).data
    except models.Statistics.DoesNotExist:
        data = None
    return Response(data)


# return dsrs with start month equal to selected month
@api_view(['GET'])
def date_range_month(request, pk):
    try:
        stats = models.Statistics.objects.filter(dsr__date_start__month=pk)
        data = serializers.StatisticsSerializer(stats, many=True).data
    except models.Statistics.DoesNotExist:
        data = None
    return Response(data)


# get the stats of the dsrs between the start and end dates
@api_view(['GET'])
def date_range(request, start, end):
    try:
        stats = models.Statistics.objects.filter(
                dsr__date_start__gte=start).filter(
                dsr__date_end__lte=end)
        data = serializers.StatisticsSerializer(stats,
                                                many=True).data
    except models.Statistics.DoesNotExist:
        data = None
    return Response(data)
