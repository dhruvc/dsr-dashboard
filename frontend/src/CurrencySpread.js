import React, { Component } from 'react';
import { PieChart, Pie, Cell, Tooltip, Legend} from 'recharts';
import './App.css';

export class CurrencySpread extends Component {


    render() {
        return (
                <PieChart width={800} height={400}>
                    <Pie data={this.props.currency_spread} dataKey="count" cx={500} cy={200} innerRadius={40} outerRadius={80} fill="#82ca9d" label>
                        <Cell fill='#0088FE'/>
                        <Cell fill='#00C49F'/>
                        <Cell fill='#FFBB28'/>
                    </Pie>
                    <Tooltip />
                    <Legend />
                </PieChart>
        );
    }
}
