import React from 'react';
import 'react-selectize/themes/index.css';
import {SimpleSelect} from 'react-selectize';


export class PeriodSelector extends React.Component {
    constructor(props) {
        super(props);

        this.handlePeriodChange = this.handlePeriodChange.bind(this);
    }

    state = {
        periods:  [],
        selected_period: ''
    }


    handlePeriodChange(code){
        if (code === undefined) {
            code = {
                label: "Monthly",
                value: 1
            }
        }
        this.props.onPeriodSelect(code);
    }

    render() {
        return (
            <SimpleSelect
                placeholder="Select a periodicity" onValueChange={value => this.handlePeriodChange(value)}
                theme="material"
                hideResetButton="True"
                options={this.props.periods}
                >
            </SimpleSelect>

        )
    }

}
