import React from 'react';
import 'react-selectize/themes/index.css';
import {SimpleSelect} from 'react-selectize';


export class TerritorySelector extends React.Component {
    constructor(props) {
        super(props);

        this.handleTerritoryChange = this.handleTerritoryChange.bind(this);
    }

    state = {
        territories:  [],
        selected_territory: ''
    }


    handleTerritoryChange(code){
        if (code === undefined) {
            code = {
                label: "Spain",
                value: "1"
            }
        }

        this.props.onTerritorySelect(code);
    }

    render() {
        return (
            <SimpleSelect
                placeholder="Select a territory" onValueChange={value => this.handleTerritoryChange(value)}
                theme="material"
                hideResetButton="True"
                options={this.props.territories}
                >
            </SimpleSelect>
        )
    }

}
