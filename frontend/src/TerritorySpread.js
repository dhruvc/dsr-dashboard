import React, { Component } from 'react';
import { PieChart, Pie, Cell, Tooltip, Legend} from 'recharts';
import './App.css';

export class TerritorySpread extends Component {


    render() {
        return (
                <PieChart width={800} height={400}>
                    <Pie data={this.props.territory_spread} dataKey="count" cx={500} cy={200} innerRadius={90} outerRadius={130} fill="#82ca9d">
                        <Cell fill='#0088FE'/>
                        <Cell fill='#00C49F'/>
                        <Cell fill='#FFBB28'/>
                        <Cell fill='#FF8042'/>
                        <Cell fill='#8dd1e1'/>
                        <Cell fill='#a4de6c'/>
                    </Pie>
                    <Tooltip />
                    <Legend/>
                </PieChart>
        );
    }
}
