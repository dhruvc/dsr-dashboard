import React, { Component } from 'react';
import './App.css';
import {DateRangeSelector} from './DateRangeSelector.js';
import {DataKeySelector} from './DataKeySelector.js';
import {CurrencySelector} from './CurrencySelector.js';
import {TerritorySelector} from './TerritorySelector.js';
import {PeriodSelector} from './PeriodSelector.js';

import {DateChart} from './DateChart.js';
import {CurrencyChart} from './CurrencyChart.js';
import {TerritoryChart} from './TerritoryChart.js';
import {PeriodChart} from './PeriodChart.js';

import {CurrencySpread} from './CurrencySpread.js';
import {TerritorySpread} from './TerritorySpread.js';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class App extends Component {
    constructor(props) {
        super(props);

        this.handle_date_ranges = this.handle_date_ranges.bind(this);
        this.handle_data_key = this.handle_data_key.bind(this);
        this.handle_currency_change = this.handle_currency_change.bind(this);
        this.handle_territory_change = this.handle_territory_change.bind(this);
        this.handle_period_change = this.handle_period_change.bind(this);
    }

    state = {
        dsrs: [],
        dsrs_curr: [],
        dsrs_peri: [],
        dsrs_terr: [],
        datakey: 'resources',
        currencies: [],
        selected_currency: '',
        territories: [],
        selected_territory: '',
        periods: [],
        selected_period: '',
        currency_spread: [],
        territory_spread: [],
        start_date: "",
        end_date: ""
    };

    async componentDidMount() {
        try {
            // fetch all starting data from the api
            const res = await fetch('http://127.0.0.1:8000/api/stats');
            const curs = await fetch('http://127.0.0.1:8000/api/currencies');
            const ters = await fetch('http://127.0.0.1:8000/api/territories');
            const pers = await fetch('http://127.0.0.1:8000/api/periods');

            const ter_spread = await fetch('http://127.0.0.1:8000/api/dsr_territories');
            const cur_spread = await fetch('http://127.0.0.1:8000/api/dsr_currencies');

            // await response
            const dsrs = await res.json();
            const curr_spread = await cur_spread.json();
            const terr_spread = await ter_spread.json();

            // set initial filters for other graphs to all
            const dsrs_curr = dsrs;
            const dsrs_terr = dsrs;
            const dsrs_peri = dsrs;

            // await response
            const currs = await curs.json();
            const terrs = await ters.json();
            const peris = await pers.json();

            // map currencies territories and periods
            // to format digestable by charts
            var currencies = currs.map(function(obj) {
                return {
                    label: obj.name,
                    value: obj.code
                }
            });

            var territories = terrs.map(function(obj) {
                return {
                    label: obj.name,
                    value: obj.id
                }
            });

            var periods = peris.map(function(obj) {
                return {
                    label: obj.name,
                    value: obj.id
                }
            });

            // count instances of currency and territory
            // put in correct format for chart
            var c_spread = curr_spread.reduce(function(sums,entry){
               sums[entry.code] = (sums[entry.code] || 0) + 1;
               return sums;
            },{});

            var currency_spread = [];
            var territory_spread = [];

            for (var prop in c_spread) {
                var count = {
                    name: prop,
                    count: c_spread[prop]
                }

                currency_spread.push(count);
            }

            var t_spread = terr_spread.reduce(function(sums,entry){
               sums[entry.name] = (sums[entry.name] || 0) + 1;
               return sums;
            },{});

            for (var pr in t_spread) {
                var co = {
                    name: pr,
                    count: t_spread[pr]
                }

                territory_spread.push(co);
            }

            this.setState({
                dsrs,
                dsrs_curr,
                dsrs_terr,
                dsrs_peri,
                currencies,
                territories,
                periods,
                currency_spread,
                territory_spread
            });
        } catch (e) {
            console.log(e);
        }
    }

    // return dsrs from a range of dates
    async handle_date_ranges(start, end) {
        var api_string = 'http://127.0.0.1:8000/api/date/';
        var call_string = api_string + start + '/' + end + '/' ;

        try {
            const res = await fetch(call_string);
            const dsrs = await res.json();
            this.setState({
                dsrs
            });
        } catch (e) {
            console.log(e);
        }
    }

    // retrieve dsrs filtered by specific currency
    async handle_currency_change(selected_currency) {
        var api_string = 'http://127.0.0.1:8000/api/currency/';
        console.log(selected_currency.value);
        var cur_string = selected_currency.value;
        var call_string = api_string + cur_string;
        try {
            const res = await fetch(call_string);
            const dsrs_curr = await res.json();
            this.setState({
                dsrs_curr,
                selected_currency
            });
        } catch (e) {
            console.log(e);
        }
    }

    // retrieve dsrs filtered by specific territory
    async handle_territory_change(selected_territory) {
        var api_string = 'http://127.0.0.1:8000/api/territory/';
        var cur_string = selected_territory.value;
        var call_string = api_string + cur_string;
        try {
            const res = await fetch(call_string);
            const dsrs_terr = await res.json();
            this.setState({
                dsrs_terr,
                selected_territory
            });
        } catch (e) {
            console.log(e);
        }
    }

    // retrieve dsrs filtered by specific periodicity
    async handle_period_change(selected_period) {
        var api_string = 'http://127.0.0.1:8000/api/period/';
        var cur_string = selected_period.value;
        var call_string = api_string + cur_string;
        try {
            const res = await fetch(call_string);
            const dsrs_peri = await res.json();
            this.setState({
                dsrs_peri,
                selected_period
            });
        } catch (e) {
            console.log(e);
        }
    }

    // update the statistics key that filters all the bar charts
    handle_data_key(datakey) {
        this.setState({
            datakey
        });
    }

    render() {
        return (
                <div className="grid">

                    <AppBar className="title" position="sticky" color="secondary">
                      <Toolbar>
                        <Typography variant="title" color="inherit">
                            <div>
                                <h1>DSRadmin</h1>
                            </div>
                        </Typography>
                        <div className="menu-stats">
                            <DataKeySelector
                                onDataKeySelect={this.handle_data_key} />
                        </div>
                      </Toolbar>
                    </AppBar>

                    <div className="menu1">
                        <DateRangeSelector
                            onDateRangeSelect={this.handle_date_ranges}/>
                    </div>
                    <div className="menu2">
                        <CurrencySelector
                            onCurrencySelect={this.handle_currency_change}
                            currencies={this.state.currencies} />
                    </div>

                    <div className="menu3">
                        <TerritorySelector
                            onTerritorySelect={this.handle_territory_change}
                            territories={this.state.territories} />
                    </div>
                    <div className="menu4">
                        <PeriodSelector
                            onPeriodSelect={this.handle_period_change}
                            periods={this.state.periods} />
                    </div>

                    <div className="date-graph">
                        <h2>Date Chart</h2>
                        <DateChart
                            dsrs={this.state.dsrs}
                            datakey={this.state.datakey}/>
                    </div>
                    <div className="cur-graph">
                        <h2>Currency Chart</h2>
                        <CurrencyChart
                            dsrs_curr={this.state.dsrs_curr}
                            datakey={this.state.datakey}/>
                    </div>
                    <div className="ter-graph">
                        <h2>Territory Chart</h2>
                        <TerritoryChart
                            dsrs_terr={this.state.dsrs_terr}
                            datakey={this.state.datakey}/>
                    </div>
                    <div className="per-graph">
                        <h2>Periodicity Chart</h2>
                        <PeriodChart
                            dsrs_peri={this.state.dsrs_peri}
                            datakey={this.state.datakey}/>
                    </div>

                    <div className="curspread">
                        <h2>Currency Spread</h2>
                        <CurrencySpread
                            currency_spread={this.state.currency_spread}/>
                    </div>
                    <div className="terspread">
                        <h2>Territory Spread</h2>
                        <TerritorySpread
                            territory_spread={this.state.territory_spread}/>
                    </div>
                </div>
        );
    }
}

export default App;
