import React from 'react';
import 'react-selectize/themes/index.css';
import {SimpleSelect} from 'react-selectize';

export class CurrencySelector extends React.Component {
    constructor(props) {
        super(props);

        this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
    }

    state = {
        currencies:  [{label: "EUR", value:"euro"}, {label: "GBP", value:"pounds"}],
        selected_currency: 'EUR'
    }


    handleCurrencyChange(code){
        if (code === undefined) {
            code = {
                label: "Euros",
                value: "EUR"
            }
        }
        this.props.onCurrencySelect(code);
    }

    render() {
        return (
            <SimpleSelect
                placeholder="Select a currency" onValueChange={value => this.handleCurrencyChange(value)}
                theme="material"
                hideResetButton="True"
                options={this.props.currencies}
                >
            </SimpleSelect>

        )
    }

}
