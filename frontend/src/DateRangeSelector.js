import React from 'react';
import 'react-selectize/themes/index.css';
import 'bootstrap-daterangepicker/daterangepicker.css'
import Button from '@material-ui/core/Button';
import DateRangePicker from 'react-bootstrap-daterangepicker';

export class DateRangeSelector extends React.Component {
    constructor(props) {
        super(props);

        this.handleDateChange = this.handleDateChange.bind(this);
    }


    handleDateChange(event, picker){
        var start = picker.startDate.format('YYYY-MM-DD');
        var end = picker.endDate.format('YYYY-MM-DD');
        this.props.onDateRangeSelect(start, end);
    }

    render() {
        return (
                <DateRangePicker
                    startDate="1/1/2017"
                    endDate="31/12/2017"
                    onApply={this.handleDateChange}>
                  <Button
                      color="inherit"
                      variant="outlined">
                      Select Date Ranges
                  </Button>
                </DateRangePicker>
        )
    }

}
