import React from 'react';
import 'react-selectize/themes/index.css';
import {SimpleSelect} from 'react-selectize';


export class DataKeySelector extends React.Component {
    constructor(props) {
        super(props);

        this.handleDataKeyChange = this.handleDataKeyChange.bind(this);
    }


    handleDataKeyChange(val){
        this.props.onDataKeySelect(val.value);
    }

    render() {
        return (
            <SimpleSelect
                placeholder="Select statistics type" onValueChange={value => this.handleDataKeyChange(value)}
                theme="default"
                hideResetButton="True">
              <option value = "resources">Resources</option>
              <option value = "release">Release</option>
              <option value = "release_transactions">Release Transactions</option>
              <option value = "net_revenue">Net Revenue</option>
              <option value = "count_sales">Count Sales</option>
              <option value = "free_units">Free Units</option>
            </SimpleSelect>

        )
    }

}
