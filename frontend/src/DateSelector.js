import React from 'react';
import 'react-selectize/themes/index.css';
import {SimpleSelect} from 'react-selectize';

export class DateSelector extends React.Component {
    constructor(props) {
        super(props);

        this.handleDateChange = this.handleDateChange.bind(this);
    }


    handleDateChange(months){
        var api_string = 'http://127.0.0.1:8000/api/date-range/';
        var month_string = months.value;
        var call_string = api_string + month_string;
        this.props.onDateSelect(call_string);
    }
    render() {
        return (
            <SimpleSelect
                placeholder="Select a month" onValueChange={value => this.handleDateChange(value)}
                theme="material"
                hideResetButton="True">
              <option value = "all">All</option>
              <option value = "1">January</option>
              <option value = "2">Feburary</option>
              <option value = "3">March</option>
              <option value = "4">April</option>
              <option value = "5">May</option>
              <option value = "6">June</option>
              <option value = "7">July</option>
              <option value = "8">August</option>
              <option value = "9">September</option>
              <option value = "10">October</option>
              <option value = "11">November</option>
              <option value = "12">December</option>
            </SimpleSelect>
        )
    }

}
