import React, { Component } from 'react';
import { CartesianGrid,
        XAxis, YAxis, Tooltip, BarChart, Label,
        Bar} from 'recharts';
import './App.css';

export class PeriodChart extends Component {
    render() {
        return (
                <BarChart width={800} height={400}
                    data={this.props.dsrs_peri}
                    key={this.props.datakey}
                    margin={{ top: 10, right: 10, left: 40, bottom: 30 }}>
                    <Bar dataKey={this.props.datakey} fill="#2F5EC4"/>
                    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                    <XAxis dataKey="dsr">
                        <Label value={"DSR ID"} offset={-8} position="insideBottom"/>
                    </XAxis>
                    <YAxis>
                    </YAxis>
                    <Tooltip />
                </BarChart>
        );
    }
}
